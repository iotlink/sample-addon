﻿using IOTLinkAPI.Addons;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.Events;
using SampleAddon.Common;
using System.Dynamic;

namespace SampleAddon.Agent
{
    public class SampleAddonAgent : AgentAddon
    {
        public override void Init(IAddonManager addonManager)
        {
            base.Init(addonManager);

            OnConfigReloadHandler += OnConfigReload;
            OnAgentRequestHandler += OnAgentRequest;
        }

        private void OnConfigReload(object sender, ConfigReloadEventArgs e)
        {
            /**
             * IOT Link call this event when there is a system configuration change.
             * There are two types of changes:
             * - Engine: User has changed main configuration file.
             * - Addon: User has changed any configuration you used the method
             * ConfigHelper.SetReloadHandler()
             **/
            LoggerHelper.Verbose("SampleAddonAgent::OnConfigReload");
        }

        private void OnAgentRequest(object sender, AgentAddonRequestEventArgs e)
        {
            /**
             * This event is called when your Service Addon requested any information
             * from the agent.
             * 
             * The agent should be used only when you need to use APIs which requires
             * a form or a loggedin session. Otherwise, keep your code on Service.
             **/
            LoggerHelper.Verbose("SampleAddonAgent::OnAgentRequest");

            AddonRequestType requestType = e.Data.requestType;

            switch (requestType)
            {
                case AddonRequestType.REQUEST_MY_REQUEST:
                    SampleRequest();
                    break;

                default: break;
            }
        }

        private void SampleRequest()
        {
            /**
             * This is an example of how can you handle and answer a request.
             **/
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_MY_REQUEST;
            addonData.requestData = "My Response";
            GetManager().SendAgentResponse(this, addonData);
        }
    }
}
